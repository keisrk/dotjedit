jEdit Settings
==============

My personal jEdit settings. Requires the Fira Code font and Nano plugin.

.. code:: bash

   $ cp -rf ${PWD}/* ${HOME}/.jedit
   $ ln -s /usr/share/jedit/modes/javascript.xml ${HOME}/.jedit/modes/javascript.xml
